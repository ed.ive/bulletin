import os
from flask import Flask, request, render_template, jsonify

# Support for gomix's 'front-end' and 'back-end' UI.
app = Flask(__name__, static_folder='public', static_url_path='')

# Set the app secret key from the secret environment variables.
app.secret = os.environ.get('SECRET')


@app.route('/')
def homepage():
    """Displays the homepage."""
    return app.send_static_file('index.html')
    
@app.route('/feed', methods=['GET', 'POST'])
def post():
  
    if request.method == 'POST':
      with open('public/index.html', 'a') as f:
        f.write("\n<p>" + request.form['message'] + "</p>")
    return app.send_static_file('index.html')

if __name__ == '__main__':
    app.run()